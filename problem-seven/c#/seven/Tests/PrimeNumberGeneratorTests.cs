﻿namespace seven.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class PrimeNumberGeneratorTests
    {
        private IPrimeNumberGenerator _primeNumberGenerator;

        [SetUp]
        public void SetUp()
        {
            _primeNumberGenerator = new PrimeNumberGenerator();
        }

        [Test]
        public void when_asked_for_the_first_prime_number_then_return_two()
        {
            var primeNumber = _primeNumberGenerator.GetPrime(1);

            Assert.That(primeNumber, Is.EqualTo(2));
        }

        [Test]
        public void when_asked_for_the_second_prime_number_then_return_three()
        {
            var primeNumber = _primeNumberGenerator.GetPrime(2);

            Assert.That(primeNumber, Is.EqualTo(3));
        }

        [Test]
        public void when_asked_for_the_third_prime_number_then_return_five()
        {
            var primeNumber = _primeNumberGenerator.GetPrime(3);

            Assert.That(primeNumber, Is.EqualTo(5));
        }
    }
}